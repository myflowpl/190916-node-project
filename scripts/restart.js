const { resolve } = require('path');
const sh = require('shelljs');
const cwd = resolve(__dirname, '..');
const projectDir = resolve(__dirname, '../server');
const projectPm2File = resolve(projectDir, 'pm2.json');

// sprawdzamy czy istnieje plik pm2.json
sh.cd(projectDir);
sh.cp('-u', resolve(cwd, 'scripts', 'pm2.json'), projectPm2File);

sh.exec(`pm2 startOrGracefulReload ${projectPm2File}`)
