const { resolve } = require('path');
const sh = require('shelljs');
const cwd = resolve(__dirname, '..');
const projectDir = resolve(__dirname, '../server');
const releaseName = new Date().toJSON().replace(/:/g, '_');
const releaseDir = resolve(projectDir, 'releases', releaseName);
const releaseFile = resolve(projectDir, 'releases', releaseName, 'RELEASE_ID');
const currentDir = resolve(projectDir, 'current');

console.log(projectDir, releaseDir, releaseFile)
// process.exit(0);

// kopiujemy pliki projektu
sh.cp('-fr', resolve(cwd, 'dist'), resolve(releaseDir));

// kopiujemy package-lock zeby zainstalować zaleznosci
sh.cp(resolve(cwd, 'package.json'), resolve(releaseDir, 'package.json'));
sh.cp(resolve(cwd, 'package-lock.json'), resolve(releaseDir, 'package-lock.json'));

// instalujemy zależności produkcyjne
// sh.cd(releaseDir).exec('npm i --production'); 

// dodajemy plik do projektó w którym
sh.echo(releaseName).to(releaseFile); 

// linkujemy do katalogu current
sh.ln('-sf', releaseDir, currentDir);

// restart
// require('./restart');
