import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { UserRegisterRequestDto, UserRegisterResponseDto, UserLoginRequestDto, UserLoginResponseDto } from '../src/user/dto';
import { TokenPayloadModel } from '../src/user/models';
import { ConfigService } from '../src/config';
import { GetCommentsRequestDto, GetCommentsResponseDto } from '../src/comments/dto';

describe('CommentsController (e2e)', () => {
  let app, config: ConfigService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    config = app.get(ConfigService);
    await app.init();
  });

  it('/comments (GET)', () => {
    const queryParams: Partial<GetCommentsRequestDto> = {
      pageIndex: 0,
      pageSize: 5,
    };
    const resBody: GetCommentsResponseDto = {
      data: expect.any(Array),
      pageIndex: 0,
      pageSize: 5,
      query: expect.any(Object),
      total: 20,
    };
    return request(app.getHttpServer())
      .get('/comments')
      .query(queryParams)
      .expect(200)
      .then(res => {
        console.log('res', res.body);
        expect(res.body).toMatchObject(resBody);
      });
  });

  // it('/comments POST', () => {

  //   const req: UserLoginRequestDto = {
  //     email: 'piotr@myflow.pl',
  //     password: '123',
  //   };
  //   const resBody: UserLoginResponseDto = {
  //     token: expect.any(String),
  //     user: {
  //       id: expect.any(Number),
  //       name: 'Piotr',
  //       email: 'piotr@myflow.pl',
  //     },
  //   };
  //   return request(app.getHttpServer())
  //     .post('/user/login')
  //     .send(req)
  //     .expect(201)
  //     .then(res => {
  //       console.log(res.body.token);
  //       expect(res.body).toMatchObject(resBody);
  //     });
  // });

});
