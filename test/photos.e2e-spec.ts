import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { readFileSync } from 'fs';
import { resolve } from 'path';

describe('PhotosController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  /**
   * upload photo
   */
  it('should upload photo', () => {
    const fileName = 'logo.png';
    const resBody = {
      thumb: expect.any(Object),
      file: expect.any(Object),
      body: expect.any(Object),
      avatar: expect.any(Object),
    };
    return request(app.getHttpServer())
      .post('/photos/upload-user-avatar')
      .field('myFile', 'my-file-name.png')
      .attach('file', readFileSync(resolve('./src/fixtures/' + fileName)), fileName)
      .then(res => {
        expect(res.body).toMatchObject(resBody);
        expect(res).toHaveProperty('statusCode', 201);
      });
  });
});
