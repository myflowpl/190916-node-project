const express = require('express')
const app = express()
const bodyParser = require('body-parser');

let comments = [
  { id: 1, name: 'Hydrogen' },
  { id: 2, name: 'Helium' },
  { id: 3, name: 'Lithium' },
  { id: 4, name: 'Beryllium' },
  { id: 5, name: 'Boron' },
  { id: 6, name: 'Carbon' },
  { id: 7, name: 'Nitrogen' },
  { id: 8, name: 'Oxygen' },
  { id: 9, name: 'Fluorine' },
  { id: 10, name: 'Neon' },
  { id: 11, name: 'Sodium' },
  { id: 12, name: 'Magnesium' },
  { id: 13, name: 'Aluminum' },
  { id: 14, name: 'Silicon' },
  { id: 15, name: 'Phosphorus' },
  { id: 16, name: 'Sulfur' },
  { id: 17, name: 'Chlorine' },
  { id: 18, name: 'Argon' },
  { id: 19, name: 'Potassium' },
  { id: 20, name: 'Calcium' }
].reverse();

// app.get('/api/comments', (req, res) => {
//   const pageIndex = req.query.pageIndex || 0;
//   const pageSize = req.query.pageSize || 5;
//   const data = comments.slice(pageIndex * pageSize).slice(0, pageSize);

//   res.json({
//     pageIndex,
//     pageSize,
//     total: comments.length,
//     data
//   })
// })

// app.post('/api/comments',
//   bodyParser.json(),
//  (req, res) => {
//   const data = {
//     id: comments.length+1,
//     name: req.body.name
//   }
//   comments.unshift(data);
//   res.json({
//     total: comments.length,
//     data
//   })
// })

// app.delete('/api/comments/:id', (req, res) => {
//   const id = parseInt(req.params.id, 10);
//   comments = comments.filter(row => row.id !== id)
//   res.json({
//     total: comments.length,
//     id
//   })
// })


// app.get('/', function (req, res) {
//   console.log('REQUEST', req.url);
//   res.send('hello world')
// })



app.get('/user', 

  function (req, res, next) {    // logger middleware
    console.log('LOG', req.url);
    next();
  },

  function (req, res, next) {    // auth middleware
    req.user = req.query.name;
    next();
  },

  function (req, res, next) {    // guard middleware
    // (req.user) ? next() : next('forbidden');
    if (req.user) { next() } else { throw new Error('clog', 'FORBIDDEN')}
  },

  function (req, res) {          // request handler
    res.send('Hi ' + req.user)
  },

  function (err, req, res, next) {    // error handler
    res.statusCode = 201;
    res.json({status: 500, err: err.message})
  },
)

app.get('/users/:userId/books/:bookId', function (req, res) {
  // res.send(req.params)
  res.statusCode = 201;
  res.end();
})

exports.app = app;


// app.listen(3000, () => console.log(`Example app listening on port 3000!`));
