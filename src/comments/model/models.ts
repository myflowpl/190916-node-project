import { ApiModelProperty } from '@nestjs/swagger';

export class CommentModel {

  @ApiModelProperty({
    example: '55',
    required: false,
  })
  id: number;
  
  @ApiModelProperty({
    example: 'Piotr',
  })
  name: string;
}
