import { CommentModel } from '../model';
import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

export class GetCommentsRequestDto {
  @ApiModelProperty({
    example: 'search string example',
    required: false,
  })
  search: string;
  @ApiModelPropertyOptional()
  @IsNumber()
  pageIndex?: number;
  @ApiModelPropertyOptional()
  pageSize?: number;
}

export class GetCommentsResponseDto {
  @ApiModelPropertyOptional()
  pageIndex: number;
  @ApiModelProperty()
  pageSize: number;
  @ApiModelProperty()
  total: number;

  @ApiModelProperty({
    isArray: true,
    type: CommentModel,
  })
  data: CommentModel[];

  @ApiModelProperty()
  query: GetCommentsRequestDto;
}

export class GetCommentResponseDto {
  total: number;
  data: CommentModel;
}
