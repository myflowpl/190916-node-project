import { UserModel } from '../models';
import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, MinLength, IsEmail } from 'class-validator';

export class UserRegisterRequestDto {
  @ApiModelProperty({example: 'Piotr'})
  name: string;
  @ApiModelProperty({example: 'piotr@myflow.pl'})
  email: string;
  @ApiModelProperty({example: '123'})
  password: string;
  
  // @ApiModelProperty({})
  // user: UserModel;

}

export class UserRegisterResponseDto {
  @ApiModelProperty()
  user: UserModel;
}

export class UserLoginRequestDto {

  @IsEmail()
  @ApiModelProperty({example: 'piotr@myflow.pl'})
  email: string;

  @IsString()
  @MinLength(3)
  @ApiModelProperty({example: '123'})
  password: string;
}

export class UserLoginResponseDto {
  @ApiModelProperty()
  token: string;
  @ApiModelProperty()
  user: UserModel;
}
