import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserRole } from '../models';
import { ROLES_NAME } from '../decorators/roles.decorator';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private readonly reflector: Reflector) { }

  canActivate(context: ExecutionContext): boolean {
    console.log('GUARD')
    const request = context.switchToHttp().getRequest();

    if (!request.tokenPayload) {
      return false;
    }
    const roles = this.reflector.get<UserRole[]>(ROLES_NAME, context.getHandler());
    if (!roles) {
      return true;
    }

    const user = request.tokenPayload.user;
    const hasRole = () => !!user.roles.find(role => !!roles.find(item => item === role));
    return user && user.roles && hasRole();
  }
}
