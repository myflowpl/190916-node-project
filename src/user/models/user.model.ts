import { ApiModelProperty, ApiModelPropertyOptional } from '@nestjs/swagger';

export enum UserRole {
  ADMIN = 'admin',
  ROOT = 'root',
}
export class UserModel {

  @ApiModelPropertyOptional()
  id?: number;
  @ApiModelProperty({example: 'Piotr'})
  name: string;
  @ApiModelPropertyOptional({example: 'piotr@myflow.pl'})
  email?: string;
  @ApiModelPropertyOptional({example: '123'})
  password?: string;

  @ApiModelPropertyOptional({
    isArray: true,
    enum: UserRole,
  })
  roles?: UserRole[];
}

export class TokenPayloadModel {
  user: UserModel;
}
