import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { ConfigService } from '../../config';

@Injectable()
export class UserInterceptor implements NestInterceptor {

  constructor(private config: ConfigService) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    console.log('INTERCEPT')

    const req = context.switchToHttp().getRequest();

    const now = Date.now();
    return next
      .handle()
      .pipe(
        map(res => {
          delete res.user.password;
          return res;
        }),
        tap(() => console.log(`After... ${Date.now() - now}ms`)),
      );
  }
}