import { SetMetadata } from '@nestjs/common';

export const ROLES_NAME = 'roles';

export const Roles = (...args: string[]) => SetMetadata(ROLES_NAME, args);
