import { TokenPayloadModel } from './models';

declare module 'express' {
  export interface Request {
    tokenPayload?: TokenPayloadModel;
  }
}
