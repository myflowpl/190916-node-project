import { Module, Global, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { logger } from './middlewares/logger.middlieware';
import { UserMiddleware } from './middlewares/user.middleware';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './guards/auth.guard';
import { AuthService } from './services/auth.service';
import { ConfigModule } from '../config';

@Global()
@Module({
  imports: [ConfigModule],
  controllers: [UserController],
  providers: [
    UserService,
    AuthService,
    // {
    //   provide: APP_GUARD,
    //   useClass: AuthGuard,
    // },
  ],
  exports: [UserService],
})
export class UserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(logger, UserMiddleware)
      .forRoutes(UserController);
  }
}
