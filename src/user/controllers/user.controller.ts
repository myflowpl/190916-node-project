import { Controller, Post, Body, Get, Request, UseGuards, HttpException, HttpStatus, UseInterceptors, UsePipes, ValidationPipe, Param } from '@nestjs/common';

import { UserService, AuthService } from '../services';
import { ApiCreatedResponse, ApiUseTags, ApiBearerAuth, ApiImplicitParam } from '@nestjs/swagger';
import { UserRegisterResponseDto, UserRegisterRequestDto, UserLoginRequestDto, UserLoginResponseDto } from '../dto';
import { User } from '../decorators/user.decorator';
import { Roles } from '../decorators/roles.decorator';
import { UserRole, UserModel } from '../models';
import { AuthGuard } from '../guards/auth.guard';
import { of, throwError, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { UserInterceptor } from '../interceptors/user.interceptor';
import { UserByIdPipe } from '../pipes/user-by-id.pipe';
import { ClientProxy, Transport, Client } from '@nestjs/microservices';

@Controller('user')
export class UserController {

  @Client({ transport: Transport.TCP, options: { port: 3001 } })
  client: ClientProxy;

  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) { }

  @Get('sum')
  sum(): Observable<number> {
    const pattern = { cmd: 'sum' };
    const payload = [1, 2, 3];
    return this.client.send<number>(pattern, payload);
  }

  @Post('login')
  @UsePipes(new ValidationPipe({ transform: true }))
  async login(@Body() credentials: UserLoginRequestDto): Promise<UserLoginResponseDto> {

    // return this.userService.findByCredentials().pipe(
    //   switchMap(user => (user ? of(user) : throwError(new HttpException('ValidationError', HttpStatus.UNPROCESSABLE_ENTITY))
    //   switchMap(user => this.authService.tokenSign(user).pipe(map(token => ({user, token}))))
    //   // map( => ())
    // );

    const user = await this.userService.findByCredentials(credentials.email, credentials.password);
    // .then(user => mapUser(user))
    // .catch(err => );
    // user = mapUser(user);

    if (!user) {
      throw new HttpException('ValidationError', HttpStatus.UNPROCESSABLE_ENTITY);
    }
    return {
      token: await this.authService.tokenSign({ user }), // .catch(err => null),
      user,
    };
  }

  @Get()
  @UseInterceptors(UserInterceptor)
  @Roles(UserRole.ADMIN)
  @UseGuards(AuthGuard)
  @ApiBearerAuth()
  getUser(@User({ extract: 'cookies' }) user) {
    console.log('REQUEST HANDLER');
    return {
      example: 'user should be here',
      user,
    };
  }

  @Post('register')
  // @ApiUseTags('user', 'auth')
  @ApiCreatedResponse({ type: UserRegisterResponseDto })
  async register(@Body() data: UserRegisterRequestDto): Promise<UserRegisterResponseDto> {
    const user = await this.userService.create(data);
    // TODO handle errors
    return {
      user,
    };
  }

  @Get(':id')
  @ApiImplicitParam({ name: 'id', type: Number })
  getUserById(@Param('id', UserByIdPipe) user: UserModel) {
    return {
      user,
    };
  }

}
