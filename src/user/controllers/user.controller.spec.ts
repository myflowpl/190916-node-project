import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService, AuthService } from '../services';
import { ConfigModule, ConfigService } from '../../config';
import { UserModule } from '../user.module';

describe('User Controller', () => {
  let controller: UserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      // controllers: [UserController],
      // providers: [UserService, AuthService],
      // imports: [ConfigModule],
      imports: [UserModule],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
