import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
// const { app: express } = require('../express/server');
// import { app as express } from '../express/server';
import { CommentsController } from './comments/controllers/comments.controller';
import { ConfigService } from './config';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.setGlobalPrefix('api');
  
  app.useStaticAssets(join(__dirname, '..', 'assets'));

  // const config = new ConfigService();
  const config = app.get(ConfigService);

  const options = new DocumentBuilder()
    .setTitle('Nest API Example')
    .setDescription('Przykładowy projekt w Node.js i TypeScript')
    .setVersion('1.0')
    .setBasePath('/api')
    .addTag('user')
    .addBearerAuth(config.TOKEN_HEADER_NAME, 'header')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  // console.log('SWAGGER', document)
  // save to /dist/swagge.json file

  SwaggerModule.setup('api/docs', app, document);

  // const ctrl = app.get<CommentsController>(CommentsController);
  // ctrl.getComments({});
  // app.use()

  await app.listen(3000);
}
bootstrap();

// @Injectable()
// export class UserService {
//   logger: LoggerService;
//   constructor(logger: LoggerService) {
//     this.logger = logger;
//   }
// }

// function Log(label) {
//   return (target, key, descriptor) => {
//     console.log('TARGET', target.name, target, key, descriptor)
//     console.log(`${label}: ${key} was called!`);
//   };
// }

// class P {
//   name = 'test name';

//   @Log('debug')
//   foo(option) {
//     console.log('FOO', option);

//   }

//   // @Log("wargning")
//   // bar() { ... }
// }
// const p = new P();
// p.foo({name: 'nest'});

// class User {
//   id: number;
//   name: string;
//   gender?: string;
// }

// async function map<U>(user: any): Promise<U> {

//   if (!user.gender) {
//     user.gender = 'F';
//   }
//   return user;
// }

// const u: User = {
//   id: 4,
//   name: 'Piotr',
// };
// const defaultUser = map<User>(u);
// defaultUser.then(uu => console.log(uu));

// interface Options {
//   label: string;
//   size?
// }

// function printLabel(options: Options) {
//   console.log(options.label);
// }

// const myObj: Options = { size: 10, label: 'Size 10 Object' };
// printLabel(myObj);
