import { Injectable } from '@nestjs/common';
import { resolve } from 'path';

@Injectable()
export class ConfigService {
  readonly JWT_SECRET = process.env.JWT_SECRET || 'jwt-secret';
  readonly TOKEN_HEADER_NAME = process.env.TOKEN_HEADER_NAME || 'api_token';

  readonly STORAGE_TMP = resolve(__dirname, '../../storage/tmp');
  readonly STORAGE_PHOTOS = resolve(__dirname, '../../storage/photos');
  readonly PHOTOS_BASE_PATH = '/photos';
  readonly STORAGE_THUMBS = resolve(__dirname, '../../assets/photos');

  readonly DB_NAME = resolve(__dirname, '../../storage/databases/db.sql');

}
