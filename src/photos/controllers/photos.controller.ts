import { Controller, Post, UseInterceptors, Body, UploadedFile } from '@nestjs/common';
import { ApiConsumes, ApiImplicitFile } from '@nestjs/swagger';
import { PhotosService } from '../services/photos.service';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('photos')
export class PhotosController {

  constructor(private photosService: PhotosService) {}

  @Post('upload-user-avatar')
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true, description: 'Upload user avatar' })
  async uploadFile(@UploadedFile() file: Express.Multer.File, @Body() body) {
    const avatar = await this.photosService.create(file);
    const thumb = await this.photosService.createThumbs(avatar.fileName);
    return {avatar, thumb, file, body};
  }
}
