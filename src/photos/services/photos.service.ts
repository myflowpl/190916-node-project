import { Injectable } from '@nestjs/common';
import { promisify } from 'util';
import { rename } from 'fs';
import { extname, join, resolve } from 'path';
const renameAsync = promisify(rename);
import * as crypto from 'crypto';
import { ConfigService } from '../../config';
import * as sharp from 'sharp';
import { InjectRepository } from '@nestjs/typeorm';
import { PhotoEntity } from '../entities';
import { Repository } from 'typeorm';

@Injectable()
export class PhotosService {

  constructor(
    private config: ConfigService,

    @InjectRepository(PhotoEntity)
    private readonly photoRepository: Repository<PhotoEntity>,
  ) {}

  async create(file: Express.Multer.File) {

    // TODO validate is photo
    const fileName = crypto
    .createHash('md5')
    .update(file.path)
    .digest('hex') + extname(file.originalname).toLowerCase();
    await renameAsync(file.path, join(this.config.STORAGE_PHOTOS, fileName));

    const photo = new PhotoEntity();
    photo.filename = fileName;
    photo.description = file.originalname;
    await this.photoRepository.save(photo);

    return {
      fileName,
      photo,
    };
  }

  async createThumbs(filename) {

    const sourceFile = resolve(this.config.STORAGE_PHOTOS, filename);
    const destFile = resolve(this.config.STORAGE_THUMBS, filename);

    await sharp(sourceFile)
      .rotate()
      .resize(200, 200, { fit: 'cover', position: 'entropy' })
      .jpeg({
        quality: 100,
      })
      .toFile(destFile).catch(err => err);

    return {
      thumbName: destFile,
    };
  }

  async findAll() {
    return this.photoRepository.find();
  }
}
