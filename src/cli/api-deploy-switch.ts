import * as path from 'path';

// const path = require('path');
const inquirer = require('inquirer');
const program = require('commander');
const sh = require('shelljs');
const chalk = require('chalk');
const { askForEnv } = require('./utils/ask-for-env');
const { listDirs } = require('./utils/list-dirs');

program
  .arguments('[version]')
  .action(downloadAction)
  .parse(process.argv);

async function downloadAction(version) {
  console.log('VVVVVVV', version)

  const BASE_DIR = 'server';
  const releasesDir = path.resolve(BASE_DIR, 'releases');
  const currentDir = path.resolve(BASE_DIR, 'current');

  const versions = listDirs(releasesDir);

  if(!version) {
    version = await inquirer.prompt([{
      type: 'list',
      name: 'version',
      default: versions[0],
      message: 'Choose version to switch to',
      choices: versions,
      validate: (val) => !!val,
    }]).then(inputs => inputs.version);
  }

  const target = path.resolve(BASE_DIR, 'releases', version);

  await sh.rm('-Rf', currentDir);
  await sh.ln('-s', target, currentDir);

  console.log(chalk.green('?'), 'switched to', chalk.blue(version));
}
