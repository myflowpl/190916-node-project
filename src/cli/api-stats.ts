
const { NestFactory } = require('@nestjs/core');
const { askForEnv } = require('./utils/ask-for-env');
const { resolve } = require('path');
const ora = require('ora');

const spinner = ora();

async function stats() {

  const env = await askForEnv();

  
  spinner.stopAndPersist({ text: 'Env selected: ' + env.ENV, prefixText: '✔' });
  spinner.start('Bootstraping...');

  const { AppModule } = require('../app.module');
  const { AppService } = require('../app.service');

  const app = await NestFactory.createApplicationContext(AppModule, {
    logger: false,
  });

  const appService = app.get(AppService);
  setTimeout(() => {

    spinner.stopAndPersist({ text: 'App bootstrapped', prefixText: '✔' });
    console.log('STATS', appService.getHello());
  }, 2000);
}
stats();
