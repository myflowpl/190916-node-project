#!/usr/bin/env node

const program = require('commander');

program
  .command('build', 'Bild ready to release package')
  .command('switch', 'Swich to new release package')
  .parse(process.argv);
